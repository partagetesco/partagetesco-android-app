package fr.partagetesco.androidapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Activité principale
 */
class MainActivity : AppCompatActivity() {
    /**
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
