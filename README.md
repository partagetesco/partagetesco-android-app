PartageTesCO - Android App

# Présentation

Application Android utilisant le service REST de PartageTesCO.

## Documentation

La documentation pour le développement est disponible :
 * [Sur Gitlab Pages](https://partagetesco.gitlab.io/partagetesco-android-app/documentation)
 * En générant la doc après avoir récupérer le code source avec la commande suivante, `./gradlew dokka` (sous GNU/Linux)
 ou ``./gradlew.bat dokka` (sous Windows). La documentation est disponible dans le dossier `build/docka/-partage-tes-c-o/index.html`.